package com.test.controller;

import com.test.dto.ResponseDTO;
import com.test.service.FileServiceIMPL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/file")
public class FileController
{
    @Autowired
    private FileServiceIMPL fileReaderService;

    // test endPoint,
    @GetMapping("/test")
    public String greetMsg()
    {
        return "Welcome to the Application";
    }

    // API - upload the file,
    @PostMapping("/fileUpload")
    public ResponseEntity<ResponseDTO> uploadFile(@RequestParam("file")MultipartFile file)
    {
        if(fileReaderService.hasCsvFormat(file))
        {
            try
            {
                fileReaderService.processAndSave(file);
                return new ResponseEntity<>(new ResponseDTO("File uploaded successfully,"+file.getOriginalFilename(),new FileController().hashCode()),HttpStatus.ACCEPTED);
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
        }
        return new ResponseEntity<>(new ResponseDTO("File uploaded failed,",new FileController().toString()),HttpStatus.BAD_REQUEST);
    }


}
