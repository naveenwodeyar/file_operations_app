package com.test.service;

import com.test.modal.Users;
import com.test.repo.FileRepo;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class FileServiceIMPL implements FileService
{
    @Autowired
    private FileRepo fileRepo;


    @Override
    public boolean hasCsvFormat(MultipartFile file)
    {
        String type = "text/csv";
            if(!type.equals(file.getContentType()))
            {
                return false;
            }
            return true;
    }

    @Override
    public void processAndSave(MultipartFile file) throws IOException
    {
        List<Users> user = csvToUsers(file.getInputStream());
        fileRepo.saveAll(user);
    }

    private List<Users> csvToUsers(InputStream inputStream) throws UnsupportedEncodingException {
        try(BufferedReader fileReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
            CSVParser parser = new CSVParser(fileReader, CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());)
        {
            List<Users> users = new ArrayList<>();
            for (CSVRecord record : parser.getRecords())
            {
                Users user = new Users(Integer.parseInt(record.get("stId")),record.get("stFirstName"),record.get("stLastName"),record.get("stEmail"),record.get("stGender"),Double.parseDouble(record.get("stIpAddress")),Long.parseLong(record.get("stClass")));
                        users.add(user);
            }
            return users;
        }
        catch (IOException e)
        {
            System.out.println(e.getLocalizedMessage());
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
