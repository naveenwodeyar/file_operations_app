package com.test.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileService
{

    boolean hasCsvFormat(MultipartFile file);

    void processAndSave(MultipartFile file) throws IOException;
}
